#!/bin/sh

REQUIRED="git"

for CMD in $REQUIRED; do
    command -v $CMD 1>/dev/null
    if [ "$?" -ne 0 ]; then
        echo "$CMD not found, aborting"; exit 1
    fi
done

git config --global user.email robinhuang123@gmail.com
git config --global user.name Akitaki
