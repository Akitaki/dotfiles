# Shadow
shadow = true;
shadow-radius = 20;
shadow-offset-x = -20;
shadow-offset-y = -20;
log-level = "warn";
shadow-exclude = [
  "name = 'Notification'",
  "class_g = 'Conky'",
  "class_g ?= 'Notify-osd'",
  "class_g = 'Cairo-clock'",
  "_GTK_FRAME_EXTENTS@:c",
  "class_g = 'Firefox' && argb",
  "!focused && !(name ~= 'polybar-main*')"
];

# Fading
fading = true;
fade-in-step = 0.1;
fade-out-step = 0.1;
fade-exclude = [];

# Opacity
inactive-opacity = 1.0;
frame-opacity = 1.0;
inactive-opacity-override = false;
inactive-dim = 0.0
opacity-rule = [
  "0:_NET_WM_STATE@[0]:32a *= '_NET_WM_STATE_HIDDEN'",
  "0:_NET_WM_STATE@[1]:32a *= '_NET_WM_STATE_HIDDEN'",
  "0:_NET_WM_STATE@[2]:32a *= '_NET_WM_STATE_HIDDEN'",
  "0:_NET_WM_STATE@[3]:32a *= '_NET_WM_STATE_HIDDEN'",
  "0:_NET_WM_STATE@[4]:32a *= '_NET_WM_STATE_HIDDEN'"
];

# Blur
blur-method = "dual_kawase";
blur-strength = 3;
blur-background-exclude = [
  "class_g = 'Firefox' && argb"
];

# Other
backend = "glx";
mark-wmwin-focused = true;
mark-ovredir-focused = true;
detect-rounded-corners = true;
detect-client-opacity = true;
refresh-rate = 0;
vsync = true;
focus-exclude = [ "class_g = 'Cairo-clock'" ];
detect-transient = true;
detect-client-leader = true;

# Prevent shadow from showing on other displays
xinerama-shadow-crop = true;

wintypes:
{
  tooltip = { fade = true; shadow = true; opacity = 0.75; focus = true; full-shadow = false; };
  dock = { shadow = true;  }
  dnd = { shadow = false; }
  popup_menu = { opacity = 0.9; }
  dropdown_menu = { opacity = 0.9; }
};
